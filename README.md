## Python

Python version > 3.X 

## Built-in Types

# List:

Arrangements are sequences of simpler information, are mutable, we can add and remove elements

Instance an empty list and checks the type:

```
>>> var = []
>>> type(var)
<class 'list'>
``` 

Functions
- count: Count the number of occurrences of a particular element in a list:
  - var.count(1)

- index: Displays the index of a given element in a list
  - var.index("letter")

- append: 
  - var.append("banana")

- remove:


# Tuples

It is similar to a list except for it is immutable, a tuple is a list of values separated by commas

Instance an tuple:

```
>>> days = ("S","M","T","W","T","F","S")
>>> type(days)
<class 'tuple'>
```

# Dictionary

Uses braces {}, contains keys and value

Instance an dictionary:

```
>>> person = {"name": "Ruan Arcega", "country": "Brazil", "state": "Santa Catarina", "city": "Joinville"}
>>> type(person)
<class 'dict'>
```

# Open files

PARAMS:
  - w: write
  - r: read
  - a: append

Create archive:

```
>>> arch = open("words.txt", "w")

```

Write in file:

```
>>> arch.write("test")
```

Append in file:

```
>>> arch = open("words.txt", "a")
>>> arch.write("test-append")
```

Read file:

```
>>> with open("words.txt") as arch:
>>>     for line in arch:
>>>         line = line.strip()
>>>         print(line)
```

Close file:

```
>>> arch.close()
```

## Comprehension


