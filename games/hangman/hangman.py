import random


def play():
    welcome()
    secret_word = run_secret_word()
    correct_letters = initialize_correct_letters(secret_word)

    hanged = False
    hit = False
    errors = 0

    while (not hanged and not hit):
        kick = kick_request()

        if (kick in secret_word):
            correct_kick_mark(kick, correct_letters, secret_word)
        else:
            errors += 1
            print('{} attempts remaining'.format(len(secret_word)-errors))

        hanged = errors == len(secret_word)
        hit = "_" not in correct_letters
        print(correct_letters)

    if (hit):
        print('You win')
    else:
        print('You lose')

# Functions

def welcome():
    print('###############################')
    print('# Welcome to the Hangman Game #')
    print('###############################')

def run_secret_word():
    arch = open("words.txt", "r")
    words = []
    for line in arch:
        line = line.strip()
        words.append(line)
    arch.close()

    number = random.randrange(0, len(words))
    secret_word = words[number].upper()

    return secret_word

def initialize_correct_letters(words):
    return ["_" for letter in words]


def kick_request():
    try:
        kick = input('Letter: ')
    except:
        pass

    kick = kick.strip().upper()
    return kick

def correct_kick_mark(kick, correct_letters, secret_word):
    for idx, letter in enumerate(secret_word):
        if (kick == letter):
            correct_letters[idx] = letter
            print('Found the letter {} in the position {}'.format(kick, idx))


if __name__ == '__main__':
    play()