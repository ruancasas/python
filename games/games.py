from hangman import hangman
from divination import divination

def choice_game():
    print('#########')
    print('# GAMES #')
    print('#########')

    print('(1) Hangman | (2) Divination')

    game = int(input('Choice a game: '))

    if (game == 1):
        print('Loading Hangman...')
        hangman.play()
    elif (game == 2):
        print('Loading Divination...')
        divination.play()

if __name__ == '__main__':
    choice_game()
