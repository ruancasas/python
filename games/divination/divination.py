import random
import sys

def play():
    print('####################################')
    print('# Welcome to the game of life!...! #')
    print('####################################')

    secret_num = random.randrange(1, 101)
    round = 1
    points = 1000

    print('Level')
    print('(1) Easy | (2) Normal | (3) Hard')

    try:
        level = int(input('Type a level: '))
    except ValueError as e:
        pass
    except KeyboardInterrupt:
        sys.exit(2)

    if (level == 1):
        attempts = 20
    elif (level == 2):
        attempts = 10
    else:
        attempts = 5

    for round in range(1, attempts + 1):
        print('attempts {} of {}'.format(round, attempts))

        try:
            n = int(input('Type a number: '))
        except ValueError as e:
            pass
        except KeyboardInterrupt:
            sys.exit(2)

        right  = (n == secret_num)
        bigger = (n > secret_num)
        less   = (n < secret_num)

        if (n < 1 or n > 100):
            print('Your should type a number between 1 - 100')
            continue

        if right:
            print('You are right, maked {} points'.format(points))
            break
        else:
            if bigger:
                print('His kick was bigger than the secret number')
            elif less:
                print('His kick was lass than the secret number')
            lost_points = abs(secret_num - n)
            points = points - lost_points

    print('Game Over!')


if __name__ == '__main__':
    play()
